/*import {v4 as uuid} from 'uuid';

export class DestinoViaje {
  selected: boolean;
  servicios: string[];
  id = uuid();
  public votes = 0;
  constructor(public nombre: string, public imagenUrl: string) {
       this.servicios = ['pileta', 'desayuno'];
  }
  setSelected(s: boolean) {
    this.selected = s;
  }
  isSelected() {
    return this.selected;
  }
  voteUp(): any {
    this.votes++;
  }
  voteDown(): any {
    this.votes--;
  }
}

*/
import { v4 as uuid } from 'uuid';
export class DestinoViaje {
  private selected: boolean;
  public servicios: string[];
  public votes: number;
  id = uuid();
  constructor(
    public nombre: string,
    public u: string,
  ) {
    this.votes = 0;
    this.servicios = ['pileta', 'desayuno'];
  }
  isSelected(): boolean {
    return this.selected;
  }
  setSelected(s: boolean) {
    this.selected == s;
  }
  voteUp() {
    this.votes += 1;
  }
  voteDown() {
    this.votes -= 1;
  }
}
